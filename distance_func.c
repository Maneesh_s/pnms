#include<stdio.h>
#include<math.h>
float input()
{
    float x;
    scanf("%f",&x);
    return x;
}
float power(float a1,float a2)
{
    float s=pow((a1-a2),2);
    return s;
}
float dist(float x,float y)
{
    float d=sqrt(x+y);
    return d;
}
void output(float x)
{
    printf("Distance is = %f \n",x);
}
int main()
{
	float x1,x2,y1,y2,p1,p2,d;
    printf("Enter 1st coordinates: \n");
    x1=input();
    y1=input();
    printf("Enter 2nd coordinates: \n");
    x2=input();
    y2=input();
    p1=power(x1,x2);
    p2=power(y1,y2);
    d=dist(p1,p2);
    output(d);
	return 0;
}
