#include <stdio.h>
int inp()
{
	int x;
	printf("Enter a number\n");
	scanf("%d\n",&x);
	return x;
}
void swap(int *a,int *b)
{
	*a=*a+*b;
	*b=*a-*b;
	*a=*a-*b;
}
void disp(int *a,int *b)
{
	printf("%d and %d\n",*a,*b);
}
int main()
{
	int a,b;
	a=inp();
	b=inp();
	disp(&a,&b);
	swap(&a,&b);
	disp(&a,&b);
	return 0;
}
