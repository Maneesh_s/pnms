#include <stdio.h>
int i_p()
{
    int a;
    scanf("%d",&a);
    return a;
}
void input(int r1,int c1,int arr[r1][c1])
{
    for(int i=0;i<r1;i++)
    {
        printf("\nEnter marks of student %d\n\n",i+1);
        for(int j=0;j<c1;j++)
        {
            printf("Enter marks for subject%d ",j+1);
            arr[i][j]=i_p();
        }
    }
}
void high(int r1,int c1,int arr[r1][c1],int max[c1])
{
    for(int i=0;i<c1;i++)
    {
        max[i]=arr[0][i];
        for(int j=0;j<r1;j++)
        {
            if(max[i]<arr[j][i])
            {
                max[i]=arr[j][i];
            }
        }
    }
}
void disp(int c1,int max[c1])
{
    for(int i=0;i<c1;i++)
    {
        printf("\nHighest in Subject %d is %d\n",i+1,max[i]);
    }
}
int main()
{
    int r,c;
    r=5;
    c=3;
    int arr[r][c],max[c];
    input(r,c,arr);
    high(r,c,arr,max);
    disp(c,max);
    return 0;
}